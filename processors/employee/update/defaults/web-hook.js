'use strict'
const employeeService = require('../../../../services/employees')
const roleService = require('../../../../services/roles')
const webHook = require('../../../../helpers/web-hook')

exports.process = async (data, context) => {
    if (!data.id) {
        throw new Error('id is required')
    }

    let log = context.logger.start(`employeeUpdate: ${data.id}`)

    let employee = await employeeService.getById(data.id, context)
    employee.role = await roleService.get({ employee: data.id }, context)

    for (const service of context.tenant.services) {
        await webHook.send('employee', 'onUpdate', employee, service, context)
    }

    log.end()
}
