
const db = require('../../../../models')
const employeeService = require('../../../../services/employees')

exports.process = async (data, context) => {
    const role = await db.role.findById(data.id).populate('employee organization')
    if ((role.status === 'active') && role.employee && role.employee.status !== 'active') {
        if (role.employee.status !== 'active') {
            db.employee.findById(role.employee.id)
            await employeeService.update({ status: 'active' }, role.employee, context)
        }
    }
}
