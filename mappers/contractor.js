'use strict'

exports.toModel = (entity) => {
    let model = {
        id: entity.id,
        code: entity.code,
        name: entity.name,
        status: entity.status
    }

    return model
}

exports.toSearchModel = (entities) => {
    return entities.map(entity => {
        return exports.toModel(entity)
    })
}
