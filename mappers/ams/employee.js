let mapper = require('../employee')

exports.toModel = entity => {
    var model = mapper.toModel(entity)
    model._id = model.id // to detect it is comming from open-age
    return model
}
