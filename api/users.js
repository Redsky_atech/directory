'use strict'
const mapper = require('../mappers/user')
const users = require('../services/users')
const roles = require('../services/roles')
const offline = require('@open-age/offline-processor')
const db = require('../models')
const activationConfig = require('config').get('activation')

exports.create = async (req) => {
    let log = req.context.logger.start('api:users:create')
    let model = req.body
    model.otp = Math.floor(Math.random() * 100000) + 100000
    let userDetails = await users.getOrCreate(model, req.context)
    userDetails.otp = model.otp
    await userDetails.save()

    let role = await roles.get({ user: userDetails }, req.context)

    userDetails.roles = [{
        id: role.id,
        code: role.code
    }]

    log.debug('user created')

    if (userDetails.phone || userDetails.email) {
        req.context.processSync = true
        await offline.queue('user', 'create', { id: userDetails.id }, req.context)
        log.debug('otp pushed')
    }

    log.end()
    return mapper.toModel(userDetails)
}

exports.verifyOtp = async (req) => {
    let logger = req.context.logger.start('verifyOtp')
    let model = req.body
    let context = req.context

    let userDetails = await users.getOrCreate(model, req.context)

    if (userDetails.otp !== Number(model.otp) && model.otp !== activationConfig.otp) {
        logger.error('invalid otp')
        throw new Error('invalid otp')
    }

    logger.debug('otp verified')

    userDetails.otp = null

    if (userDetails.phone) {
        model.isPhoneValidate = true
    }

    if (userDetails.email) {
        model.isEmailValidate = true
    }

    let updatedUser = await users.update(model, userDetails, req.context)

    context.user = updatedUser
    updatedUser.roles = await roles.search(context)

    return mapper.toModel(updatedUser)
}

exports.signIn = async (req) => {
    let model = req.body

    let existingUser = await users.get(model, req.context)

    if (!existingUser) {
        throw new Error('user not found')
    }

    if (existingUser.status !== 'active') {
        throw new Error('you have been blocked by the admin')
    }

    if (!existingUser.password) {
        throw new Error('you need to reset your password')
    }

    let isPasswordMatch = await users.comparePassword(model.password, existingUser.password)

    if (!isPasswordMatch) {
        throw new Error('Bad Password')
    }

    existingUser.roles = await db.role.find({
        user: existingUser.id,
        tenant: req.context.tenant.id
    }).populate('type user organization tenant').populate({
        path: 'employee',
        populate: {
            path: 'designation division department'
        }
    })

    //  existingUser.token = await auth.getToken(context)

    return mapper.toModel(existingUser)
}

exports.resetPassword = async (req) => {
    let log = req.context.logger.start('api/users:resetPassword')
    let model = req.body

    const user = req.context.user

    let updatedUser = await users.updatePassword(model, user, req.context)

    log.info('password successfully updated')
    log.end()
    return mapper.toModel(updatedUser)
}

exports.setPassword = async (req) => {
    let model = req.body
    model.id = req.params.id
    let user = await users.get(model, req.context)

    let otp = model.otp || model.activationCode

    if (user.otp !== Number(otp) && otp !== activationConfig.otp) {
        req.context.logger.error('Invalid verification code')
        throw new Error('Invalid verification code')
    }

    user.otp = null

    let updatedUser = await users.update({
        password: model.password
    }, user, req.context)

    req.context.user = user

    // await offline.queue('user', 'password-updated', {
    //     id: req.context.role.id
    // }, req.context)

    updatedUser.roles = await roles.search(req.context)
    return mapper.toModel(updatedUser)
}

exports.update = async (req, res) => {
    let log = req.context.logger.start('update')

    let model = req.body

    let id = req.params.id !== 'my' ? req.params.id : req.context.user.id

    try {
        if (model.code) {
            let sameCodeUser = await db.user.findOne({
                code: model.code,
                _id: {
                    $ne: id
                }
            })

            if (sameCodeUser) {
                log.end()
                return res.failure(`user with code ${model.code} already exist`)
            }
        }

        let userModel = await users.getById(id, req.context)

        let updatedUser = await users.update(model, userModel, req.context)

        if (req.params.id === 'my') {
            req.context.processSync = true // update tenants on user update

            offline.queue('role', 'update', {
                id: req.context.role.id
            }, req.context)
        }

        updatedUser.roles = await roles.search(req.context)

        log.end()
        return res.data(mapper.toModel(updatedUser))
    } catch (error) {
        log.error(error)
        return res.failure(error)
    }
}

exports.get = async (req, res) => {
    req.context.logger.start('get')

    let id = req.params.id === 'my' ? req.context.user.id : req.params.id

    let existingUser = await users.getById(id, req.context)

    req.context.user = existingUser

    existingUser.roles = await roles.search(req.context)

    return mapper.toModel(existingUser)
}

exports.search = async (req) => {
    let log = req.context.logger.start('api/users:search')

    let query = {
        tenant: req.context.tenant,
        status: {
            $ne: 'inactive'
        }
    }

    if (req.query.phone) {
        query.phone = req.query.phone
    }

    if (req.query.status) {
        query.status = req.query.status
    }

    if (req.query.name) {
        query.$or = [{
            phone: {
                $regex: '^' + req.query.name,
                $options: 'i'
            }
        }, {
            'profile.firstName': {
                $regex: '^' + req.query.name,
                $options: 'i'
            }
        }]
    }

    if (req.query.isTemporary) {
        query.isTemporary = !!(req.query.isTemporary)
    }

    if (req.query.isEmailValidate) {
        query.isEmailValidate = !!(req.query.isEmailValidate)
    }

    if (req.query.isPhoneValidate) {
        query.isPhoneValidate = !!(req.query.isPhoneValidate)
    }

    let users = await db.user.find(query)

    let userList = []
    for (let user of users) {
        let roles = await db.role.find({
            user: user.id,
            organization: { $exists: false },
            employee: { $exists: false }
        })

        user.roles = roles.map(role => {
            return {
                id: role.id,
                code: role.code
            }
        })
        userList.push(user)
    }
    log.end()
    return mapper.toSearchModel(userList)
}

exports.resendOtp = async (req) => {
    let log = req.context.logger.start('api/users:resend')

    let model = req.body
    model.otp = Math.floor(Math.random() * 100000) + 100000
    let userDetails = await users.getOrCreate(model, req.context)
    userDetails.otp = model.otp
    await userDetails.save()

    req.context.processSync = true
    await offline.queue('user', 'create', { id: userDetails.id }, req.context)
    log.debug('otp pushed')
    log.end()
    return mapper.toModel(userDetails)
}

exports.signUp = async (req) => {
    let log = req.context.logger.start('api/users/signUp')

    let model = req.body

    if (model.email) {
        let emailUser = await users.getByEmail(model.email, req.context)
        if (emailUser && emailUser.isEmailValidate) {
            throw new Error('email already exist')
        }
    }

    if (model.phone) {
        let phoneNumberUser = await users.getByPhone(model.phone, req.context)
        if (phoneNumberUser && phoneNumberUser.isPhoneValidate) {
            throw new Error('phoneNumber already exist')
        }
    }

    if (model.code) {
        let codeUser = await users.getByCode(model.code, req.context)
        if (codeUser && (codeUser.isEmailValidate || codeUser.isPhoneValidate)) {
            throw new Error('code already exist')
        }
    }

    model.otp = Math.floor(Math.random() * 100000) + 100000
    let userDetails = await users.getOrCreate(model, req.context)
    userDetails.otp = model.otp
    await userDetails.save()

    let role = await roles.get({ user: userDetails }, req.context)

    userDetails.roles = [{
        id: role.id,
        key: model.facebookId ? role.key : undefined,
        code: role.code
    }]

    log.debug('user created')

    req.context.processSync = true
    await offline.queue('user', 'create', { id: userDetails.id }, req.context)
    log.debug('otp pushed')

    log.end()
    return mapper.toModel(userDetails)
}

exports.profile = async (req) => { // update profile with otp
    let log = req.context.logger.start('api/users/profile')

    let id = req.params.id
    let user = await users.getById(id, req.context)

    if (user.otp !== req.body.otp && req.body.otp !== activationConfig.otp) {
        log.error('invalid otp')
        throw new Error('invalid otp')
    }

    if (!user.isEmailValidate && !user.isPhoneValidate && user.isTemporary) {
        return 'user profile can not be updated because either user is not verified or temporary'
    }

    let updatedUser = await users.update(req.body, user, req.context)

    log.end()
    return mapper.toModel(updatedUser)
}
