'use strict'
const logger = require('@open-age/logger')('tenants')
const mapper = require('../mappers/tenant')
const tenants = require('../services/tenants')
const users = require('../services/users')
const db = require('../models')

exports.create = async (req, res) => {
    logger.start('create')

    let model = {
        code: req.body.code,
        name: req.body.name
    }

    let tenant = await tenants.create(model)

    let context = {}
    context.tenant = tenant
    context.logger = logger

    let owner = await users.getOrCreate(req.body.owner, context)

    let role = await db.role.findOne({
        user: owner,
        tenant: tenant,
        employee: {
            $exists: false
        }
    })

    if (!role) {
        throw new Error('owner role not found')
    }

    tenant.owner = role
    tenant.save()
    return res.data(mapper.toModel(tenant))
}
