'use strict'
const mapper = require('../mappers/division')
const division = require('../services/divisions')
const logger = require('@open-age/logger')('divisions')
const db = require('../models')

exports.create = async (req, res) => {
    let model = req.body

    try {
        let newDivision = await division.create(model, req.context)

        return res.data(mapper.toModel(newDivision))
    } catch (err) {
        logger.error(err)
        res.failure(err)
    }
}

exports.search = async (req, res) => {
    logger.start('search')
    try {
        let divisions = await division.search(req.query, req.context)

        if (!divisions) {
            return res.failure('no divisions found')
        }
        res.page(mapper.toSearchModel(divisions))
    } catch (err) {
        res.failure(err)
    }
}

exports.delete = async (req) => {
    let division = await db.division.findOne({ '_id': (req.params.id).toObjectId() })

    if (!division) {
        throw new Error('division not found')
    }
    let entity = division
    entity.status = 'inactive'
    await entity.save()
    return 'division removed successfully'
}

exports.update = async (req, res) => {
    let model = req.body

    let entity = await db.division.findById(req.params.id)
    if (!entity) {
        return res.failure('division not found')
    }
    let updatedEntity = await division.update(model, entity, req.context)

    return mapper.toModel(updatedEntity)
}

exports.bulk = async (req) => {
    for (const item of req.body.items) {
        let entity
        if (item.code) {
            entity = await division.get({
                code: item.code
            }, req.context)
        }
        if (entity) {
            await division.update(item, entity, req.context)
        } else {
            await division.create(item, req.context)
        }
    }

    return `added/updated '${req.body.items.length}' item(s)`
}
