'use strict'

const logger = require('@open-age/logger')('roles')
const mapper = require('../mappers/role')
const roleService = require('../services/roles')
const roleTypes = require('../services/role-types')
const employeeService = require('../services/employees')
const offline = require('@open-age/offline-processor')
const organizationService = require('../services/organizations')
const dependentService = require('../services/dependents')

exports.get = async (req, res) => {
    logger.start('get')
    let identifier = req.params.id === 'my' ? req.context.role.id : req.params.id

    try {
        let role = identifier.isObjectId() ? await roleService.getById(identifier) : await roleService.getByCode(identifier)
        if (!role) {
            throw new Error(`invalid id ${identifier}`)
        }

        if (req.params.id !== 'my' || !identifier.isObjectId()) {
            role.key = undefined
            return res.data(mapper.toModel(role))
        }

        return res.data(mapper.toModel(role))
    } catch (error) {
        logger.error(error)
        return res.failure(error)
    }
}

exports.search = async (req, res) => {
    logger.start('search')
    try {
        let rolesList = await roleService.search(req.context)
        return res.page(mapper.toSearchModel(rolesList))
    } catch (error) {
        return res.failure(error)
    }
}

exports.create = async (req) => {
    let log = logger.start('api/roles:create')

    const user = req.context.user

    let employeeModel = req.body.employee
    employeeModel.user = user
    employeeModel.phone = employeeModel.phone || user.phone
    employeeModel.email = employeeModel.email || user.email
    employeeModel.status = 'new'
    // employeeModel.type = employeeModel.type

    let profileModel = employeeModel.profile || {}
    profileModel.firstName = profileModel.firstName || user.profile.firstName
    profileModel.lastName = profileModel.lastName || user.profile.lastName
    profileModel.gender = profileModel.gender || user.profile.gender
    profileModel.dob = profileModel.dob || user.profile.dob

    let picModel = profileModel.pic || user.profile.pic
    picModel.url = picModel.url || user.profile.pic.url || user.picUrl
    picModel.thumbnail = picModel.thumbnail || user.profile.pic.thumbnail

    profileModel.pic = picModel
    employeeModel.profile = profileModel
    let organizationModel = req.body.organization

    let organization
    let role

    if (organizationModel.id) {
        organization = await organizationService.getById(organizationModel.id, req.context)
    } else {
        organization = await organizationService.getByCode(organizationModel.code, req.context)
    }

    if (organization) {
        log.debug(`joining existing organization`)
        req.context.organization = organization

        employeeModel.role = {
            type: `${organization.type || 'organization'}.${employeeModel.type || 'employee'}`,
            status: 'inactive'
        }
        if (req.context.tenant.code === 'aqua') {
            employeeModel.type = 'normal'
            employeeModel.role = {
                type: `organization.normal`,
                status: 'inactive'
            }
        }

        let employee = await employeeService.getOrCreate(employeeModel, req.context)

        if (!employee) {
            throw new Error(`employee not found`)
        }

        role = await roleService.get({
            employee: employee,
            user: user,
            organization: organization
        }, req.context)
    } else {
        log.debug(`creating new organization`)

        if (req.context.tenant.code === 'aqua') {
            organizationModel.status = 'active'
            employeeModel.code = 'default'
            employeeModel.status = 'active'
            employeeModel.type = 'superadmin'
            employeeModel.role = {
                type: `organization.superadmin`,
                status: 'active'
            }
        }

        organizationModel.employee = employeeModel
        organization = await organizationService.create(organizationModel, req.context)

        role = organization.owner
        req.context.organization = organization
        // throw new Error('organization does not exist')
    }

    if (!role) {
        throw new Error('role not found')
    }

    role = await roleService.addExtraPermission(employeeModel.role.type, role, req.context)

    return mapper.toModel(role)

    // let role = await roleService.get({       // todo changes for student
    //     code: req.body.code,
    //     organization: req.context.organization
    // }, req.context)

    // if (role) {
    //     throw new Error(`role code ${req.body.code} already exist`)
    // }

    // let user = await userService.getOrCreate({
    //     phone: req.body.phone,
    //     email: req.body.email,
    //     profile: req.body.profile
    // }, req.context)

    // type = await roleTypes.get(req.body.type, req.context)

    // role = await roleService.getOrCreate({
    //     code: req.body.code,
    //     phone: req.body.phone,
    //     email: req.body.email,
    //     profile: req.body.profile,
    //     location: req.body.location,
    //     address: req.body.address,
    //     organization: req.context.organization,
    //     status: req.body.status,
    //     user: user,
    //     type: type
    // }, req.context)
    // /* add permission to role end */

    // // todo  student in organization
    // // todo employee roleType

    // role.key = undefined
    // return mapper.toModel(role)
}

exports.update = async (req) => {
    let log = req.context.logger.start('update')

    let id = req.params.id !== 'my' ? req.params.id : req.context.role.id

    if (req.body.code) {
        let sameCodeRole = await roleService.getByCode(req.body.code)
        if (sameCodeRole) {
            throw new Error('code exist')
        }
    }

    let existingRole = await roleService.getById(id, req.context)

    if (existingRole.isCodeUpdated) {
        throw new Error('you can update code once')
    }

    let role = await roleService.update(req.body, existingRole, req.context)

    log.end()

    return mapper.toModel(role)
}

exports.codeAvailable = async (req) => {
    let role = await roleService.getByCode(req.body.code)

    let data = {}

    data.isAvailable = !role

    return data
}

exports.createDependent = async (req) => {
    let log = req.context.logger.start('api/dependents:createDependent')

    const id = req.params.id !== 'my' ? req.params.id : req.context.role.id

    await dependentService.create(req.body, id, req.context)

    let headRoleWithDependent = await roleService.getWithDependent(id, req.context)

    log.end()
    return mapper.toModel(headRoleWithDependent)
}

exports.createDependentsInBulk = async (req) => {
    let log = req.context.logger.start('api/dependents:bulk')

    const id = req.params.id !== 'my' ? req.params.id : req.context.role.id

    const dependents = req.body.dependents || req.body.items

    for (let index = 0; index < dependents.length; index++) {
        let dependentModel = dependents[index]
        await dependentService.create(dependentModel, id, req.context)
    }

    let headRoleWithDependent = await roleService.getWithDependent(id, req.context)

    log.end()
    return mapper.toModel(headRoleWithDependent)
}
