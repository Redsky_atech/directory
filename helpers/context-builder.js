'use strict'
const db = require('../models')
const locks = require('./locks')

const create = async (claims, logger) => {
    let context = {
        logger: logger || claims.logger,
        config: {
            timeZone: 'IST'
        },
        permissions: []
    }

    let log = context.logger.start('context-builder:create')

    if (claims.tenant && claims.tenant._doc) {
        context.tenant = claims.tenant
    } else if (claims.tenant && claims.tenant.id) {
        context.tenant = await db.tenant.findOne({ _id: claims.tenant.id }).populate('owner')
    } else if (claims.tenant && claims.tenant.code) {
        context.tenant = await db.tenant.findOne({ code: claims.tenant.code }).populate('owner')
    } else if (claims.tenant && claims.tenant.key) {
        context.tenant = await db.tenant.findOne({ key: claims.tenant.key }).populate('owner')
    }

    context.setOrganization = async (organization) => {
        if (!organization) {
            return
        }
        if (organization._doc) {
            context.organization = organization
        } else if (organization.id) {
            context.organization = await db.organization.findOne({ _id: organization.id }).populate('owner')
        } else if (organization.key) {
            context.organization = await db.organization.findOne({ key: organization.key }).populate('owner')
        } else if (organization.code) {
            context.organization = await db.organization.findOne({ code: organization.code }).populate('owner')
        }

        if (context.organization.config) {
            context.config = context.organization.config
            context.config.timeZone = context.config.timeZone || 'IST'
        }
    }

    context.setUser = async (user) => {
        if (!user) {
            return
        }
        if (user._doc) {
            context.user = user
        } else if (user.id) {
            user = await db.user.findOne({ _id: user.id })
        }
    }

    context.setRole = async (role) => {
        if (!role) {
            return
        }
        if (role._doc) {
            context.role = role
        } else if (role.id) {
            context.role = await db.role.findOne({ _id: role.id }).populate('user employee organization tenant type')
        } else if (role.key) {
            context.role = await db.role.findOne({ key: role.key }).populate('user employee organization tenant type')
        } else if (role.code && context.tenant) {
            context.role = await db.role.findOne({
                code: role.code,
                tenant: context.tenant,
                organization: null // TODO: figure out if null is supported
            }).populate('user employee organization tenant')
        }

        context.role.permissions = context.role.permissions || []
        context.tenant = context.role.tenant
        context.organization = context.role.organization
        context.employee = context.role.employee
        context.user = context.role.user

        context.permissions = context.role.permissions

        if (context.role.type && context.role.type.permissions) {
            context.role.type.permissions.forEach(permission => context.permissions.push(permission))
        }
    }

    await context.setOrganization(claims.organization)
    await context.setRole(claims.role)
    await context.setUser(claims.user)

    context.getConfig = (identifier, defaultValue) => {
        var keys = identifier.split('.')
        var value = context.config

        for (var key of keys) {
            if (!value[key]) {
                return defaultValue
            }
            value = value[key]
        }

        return value
    }

    context.hasPermission = (permission) => {
        return context.permissions.find(permission)
    }

    context.where = () => {
        let clause = {}

        if (context.organization) {
            clause.organization = context.organization.id.toObjectId()
        }
        if (context.tenant) {
            clause.tenant = context.tenant.id.toObjectId()
        }
        let filters = {}

        filters.add = (field, value) => {
            if (value) {
                clause[field] = value
            }
            return filters
        }

        filters.clause = clause

        return filters
    }

    context.lock = async (resource) => {
        return locks.acquire(resource, context)
    }
    log.end()

    return context
}

exports.serializer = async (context) => {
    let serialized = {}

    if (context.role) {
        serialized.roleId = context.role.id
    }

    if (context.user) {
        serialized.userId = context.user.id
    }

    if (context.employee) {
        serialized.employeeId = context.employee.id
    }

    if (context.tenant) {
        serialized.tenantId = context.tenant.id
    }

    if (context.organization) {
        serialized.organizationId = context.organization.id
    }

    return serialized
}

exports.deserializer = async (claims, logger) => {
    let obj = {}

    if (claims.roleId) {
        obj.role = {
            id: claims.roleId
        }
    }

    if (claims.userId) {
        obj.user = {
            id: claims.userId
        }
    }

    if (claims.employeeId) {
        obj.employee = {
            id: claims.employeeId
        }
    }

    if (claims.tenantId) {
        obj.tenant = {
            id: claims.tenantId
        }
    }

    if (claims.organizationId) {
        obj.organization = {
            id: claims.organizationId
        }
    }

    return create(claims, logger)
}

exports.create = create
