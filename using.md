# how to use ed

These are set of APIs, using which you can manage organinzations and tenants

## Format of the API

The API's would be pure RESTful and can be accessed at

* Live root url https://ed.mindfulsas.com/api/v1/
* Test root url https://ed-test.m-sas.com/api/v1/
* Dev root url https://ed-dev.m-sas.com/api/v1/

### Getting a list of resources

TODO

### Getting details of a resource

TODO

## APIs

### tenants

#### signup with ed

create a new tenant inside the ED
**Request**
POST: `{{root}}/tenants`
_body_
```JavaScript
{
    code: "axon",
    name: "Axon",
    owner: {
        "name": "Sunny Parkash",
        "email": "sunny.parkash@gmail.com"
    }
}
```
**Response**
```JavaScript
{
    id: 21,
    code: "axon",
    name: "Axon",
    key: "xxxx-xxxx-xxxx-xxxx",
    owner: {
        "name": "Sunny Parkash",
        "email": "sunny.parkash@gmail.com"
    }
}
```

### organizations

#### creating new organization
**Request**
POST: `{{root}}/organizations`
_body_
```JavaScript
{
    code: "msas",
    name: "Mindful Systems and Solutions",
    owner: { // the employee who is admin
        user: {
            name: "Gurpreet Kaur",
            email: "gurpreet.kaur@mindfulsas.com",
        },
        role: {
            type: "Admin"
        }
    }
}
```
_header_
```JavaScript
{
    x-app-api-key: "xxxx-xxxx-xxxx-xxxx"
}
```

defaults will be created if not specified for division, department and designation
```JavaScript
{
    id: 21 // primary key in ed
    code: '1',
    name: 'Default'  
}
```

**Response**
```JavaScript
{
    id: 21,
    code: "msas",
    name: "Mindful Systems and Solutions",
    owner: { // the employee who is admin
        id: 21,  
        code: 1, // auto created from last employee code
        user: {
            id: 33,
            name: "Gurpreet Kaur",
            email: "gurpreet.kaur@mindfulsas.com",
        },
        designation: {
            id: 21
            code: '1',
            name: 'Default'
        },
        division: {
            id: 21
            code: '1',
            name: 'Default'
        },
        department: {
            id: 21
            code: '1',
            name: 'Default'
        },
        role: {
            type: "Owner",
            permissions: ['all'],
            api-key: "yyyy-yyyy-yyyy-yyyy",
            tenant: {
                id: 21
            }
        }
    }
}
```

#### adding hooks

**Request**
POST: `{{root}}/hooks`
```JavaScript
{
    type: 'on-employee-update',
    url: 'http://axon.mindfulsas.com/api/hooks/on-employee-update',
}
```

_header_
```JavaScript
{
    x-app-api-key: "xxxx-xxxx-xxxx-xxxx"
}
```

when an employee gets updated, it post following
```JavaScript
{
    type: 'on-employee-update',
    employee: {
        role: {
           key: 'nnnn-nnnn-nnnn-nnnn' 
        } 
    }
}
```
### employees



#### new employee
#### updating an employee

### auth with ed
#### signup

**Request**
POST: `{{root}}/users`
```JavaScript
{
    email: 'ankit.manchanda@mindfulsas.com'
    // or
    mobile: '9872766252'
}
```
a user would be created (if it does not exist) and a new otp would be generated and sent to mobile or email

to confirm using following
**Request**
POST: `{{root}}/users/confirm`

```JavaScript 
{
    email: 'ankit.manchanda@mindfulsas.com'
    // or
    mobile: '9872766252'
    otp: 1234,
    password: 'dt263535f'
}
```
_header_
```JavaScript
{
    x-app-code: "axon"
}
```

**Response**
```JavaScript 
{
    token: 'mmmm-mmmm-mmmm-mmmm', // this token identifies the user in the system
    roles: [] // if tenant code is present, it gets the roles of the user in that tenant
}
```

using the token user can register as employee in an org

#### login 
ED will support password login in one has been set, alernatively user can use OTP to set the password.

**Request**
POST: `{{root}}/users/login`

```JavaScript 
{
    email: 'ankit.manchanda@mindfulsas.com'
    // or
    mobile: '9872766252'
    password: 'dt263535f'
}
```
_header_
```JavaScript
{
    x-app-code: "axon"  
}
```

**Response**
```JavaScript 
{
    token: 'mmmm-mmmm-mmmm-mmmm', // this token identifies the user in the system
    roles: [] // if tenant code is present, it gets the roles of the user in that tenant
}
```

#### setting the password
**Request**
POST: `{{root}}/users/password`
```JavaScript 
{
    password: 'dt263535f'
}
```
_header_
```JavaScript
{
    x-access-token: 'mmmm-mmmm-mmmm-mmmm'
}
```
## roles
roles can be fetched only for an tenant

#### setting the password
**Request**
GET: `{{root}}/roles`
_header_
```JavaScript
{
    x-access-token: 'mmmm-mmmm-mmmm-mmmm',
    x-app-code: 'axon'
}
```
**Response**
the response is not paged
```JavaScript 
{
    items: [{
        id: '',
        type: '',
        key: 'nnnn-nnnn-nnnn-nnnn',
        permissions: ['owner'],
        employee: {
            code: '',
            organization: {
                code: ''
            }
        }
        // tenant: {
        //     code: 'axon'
        // }
    }]
}
```