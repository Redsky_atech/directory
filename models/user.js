'use strict'

const mongoose = require('mongoose')

module.exports = {
    phone: String,
    email: {
        type: String,
        lowercase: true
    },
    facebookId: String,
    code: String, // userName
    otp: Number,
    password: String,
    picUrl: {
        type: String
    }, // TODO: obsolete
    profile: {
        firstName: {
            type: String,
            lowercase: true
        },
        lastName: {
            type: String,
            lowercase: true
        },
        dob: Date,
        gender: {
            type: String,
            enum: ['male', 'female', 'other', 'none', 'unknown']
        },
        pic: {
            url: String,
            thumbnail: String
        }
    },
    identities: { // hash these fields
        aadhaar: {
            type: String
        },
        pan: {
            type: String
        },
        passport: {
            type: String
        }
    },
    status: {
        type: String,
        default: 'active',
        enum: ['active', 'inactive', 'blocked']
    },
    isProfileComplete: {
        type: Boolean,
        default: false
    },
    isPhoneValidate: {
        type: Boolean,
        default: false
    },
    isEmailValidate: {
        type: Boolean,
        default: false
    },
    isTemporary: {
        type: Boolean,
        default: false
    },

    tenant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tenant',
        required: true
    }
}