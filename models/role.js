'use strict'
var mongoose = require('mongoose')

module.exports = {
    code: {
        type: String,
        index: true,
        unique: true
    },
    key: {
        type: String,
        required: [true, 'role key required'],
        index: true,
        unique: true
    },
    // email: { type: String },
    // phone: { type: String },
    // profile: {
    //     firstName: String,
    //     lastName: String,
    //     pic: {
    //         url: String,
    //         thumbnail: String
    //     },
    //     dob: {
    //         type: Date,
    //         default: null
    //     },
    //     bloodGroup: String,
    //     gender: {
    //         type: String,
    //         enum: ['male', 'female', 'other']
    //     }
    // },
    // location: {
    //     coordinates: {
    //         type: [Number], // [<longitude>, <latitude>]
    //         index: '2dsphere' // create the geospatial index
    //     },
    //     name: { type: String },
    //     description: { type: String }
    // },
    // address: {
    //     line1: String,
    //     line2: String,
    //     district: String,
    //     city: String,
    //     state: String,
    //     pinCode: String,
    //     country: String
    // },
    type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'roleType',
        required: [true, 'role-type required']
    },
    status: {
        type: String,
        default: 'new',
        enum: ['in-complete', 'new', 'active', 'inactive', 'archived', 'blocked']
    },

    permissions: [{ type: String }], // additional permissions
    previousCode: String,
    isCodeUpdated: {
        type: Boolean,
        default: false
    },
    dependents: [{
        role: { type: mongoose.Schema.Types.ObjectId, ref: 'role' },
        relation: String // son, brother
    }],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: [true, 'user required']
    },
    employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'employee'
    },
    // department: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'department'
    // },
    // division: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'division'
    // },
    organization: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'organization'
    },
    tenant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tenant',
        required: [true, 'tenant required']
    }
}
