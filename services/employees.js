/* eslint-disable indent */
'use strict'

const userService = require('./users')
const divisionService = require('./divisions')
const designationService = require('./designations')
const departmentService = require('./departments')
const contractorService = require('./contractors')

const roleService = require('./roles')
const roleTypeService = require('./role-types')

const dates = require('../helpers/dates')

const db = require('../models')

const offline = require('@open-age/offline-processor')

const getNewCode = async (field, context) => {
    let lock = await context.lock(`organization:${context.organization.id}:${field}`)

    let organization = await db.organization.findById(context.organization.id)

    let newCode = (organization[field] || 0) + 1

    organization[field] = newCode

    await organization.save()

    lock.release()

    return '' + newCode
}

const getNewEmployeeCode = async (options, context) => {
    return getNewCode('lastEmployeeCode', context)
}

const updateProfile = (model, entity, context) => {
    if (model.firstName) {
        entity.firstName = model.firstName
    }

    if (model.lastName) {
        entity.lastName = model.lastName
    }

    if (model.pic && model.pic.url) {
        entity.pic = {
            url: model.pic.url
        }
    }

    if (model.dob) {
        entity.dob = model.dob
    }

    if (model.fatherName) {
        entity.fatherName = model.fatherName
    }

    if (model.bloodGroup) {
        entity.bloodGroup = model.bloodGroup
    }

    if (model.gender) {
        entity.gender = model.gender
    }

    return entity
}

const create = async (data, context) => {
    let log = context.logger.start('create')

    let employee

    if (data.code) {
        employee = await db.employee.findOne({
            code: data.code,
            organization: context.organization.id
        }).populate('user department division designation organization').populate({
            path: 'supervisor',
            populate: {
                path: 'user department division designation organization'
            }
        })
    }

    if (employee) {
        log.debug(`returning existing employee ${employee.id}`)
        log.end()
        return employee
    }

    if (!data.code) {
        data.code = await getNewEmployeeCode({}, context)
    }

    let entity = await db.employee.findOne({
        code: data.code,
        organization: context.organization.id
    })

    if (entity) {
        throw new Error(`employee with code '${data.code}' already exists`)
    }

    if (!data.email && !data.phone) {
        data.email = `${data.code}@${context.organization.code}.com`
    }

    let user = await userService.getOrCreate({
        phone: data.phone,
        email: data.email
    }, context)

    let profile = {}
    if (data.profile) {
        profile = data.profile
    } else if (data.user && data.user.profile) {
        profile = data.user.profile
    } else if (user.profile) {
        profile = user.profile
    }

    let address = {}
    if (data.address) {
        address = data.address
    } else if (data.user && data.user.address) {
        address = data.user.address
    } else if (user.address) {
        address = user.address
    }

    let config = {}
    if (data.config) {
        config = data.config
    }

    let model = {
        code: data.code,
        phone: data.phone,
        email: data.email,
        type: data.type,
        profile: profile,
        address: address,
        status: data.status || 'new',
        user: user,
        supervisor: data.supervisor,
        division: data.division,
        designation: data.designation,
        department: data.department,
        config: config,
        doj: data.doj || new Date(),
        dol: data.dol,
        reason: data.reason,
        organization: context.organization
    }

    if (data.supervisor) {
        model.supervisor = await get(data.supervisor, context)
    }

    model.division = await divisionService.get(data.division || {
        code: 'default'
    }, context)
    model.designation = await designationService.get(data.designation || {
        code: 'default'
    }, context)
    model.department = await departmentService.get(data.department || {
        code: 'default'
    }, context)

    if (model.config.contractor) {
        let contractor = await contractorService.get(model.config.contractor, context)
        if (contractor) {
            model.config.contractor = {
                id: contractor.id.toObjectId(),
                code: contractor.code,
                name: contractor.name
            }
        }
    }
    // if (!model.user) { model.user = context.user.id }

    let newEmployee = await new db.employee(model).save()

    employee = await db.employee.findById(newEmployee.id).populate('user department division designation organization').populate({
        path: 'supervisor',
        populate: {
            path: 'user department division designation organization'
        }
    })

    let roleTypeCode = `${context.organization.type || 'organization'}.${employee.type || 'employee'}`
    let roleStatus

    if (model.role) {
        if (model.role.type || model.role.code) {
            roleTypeCode = model.role.type || model.role.code
        }
        if (model.role.status) {
            roleStatus = model.role.status
        }
    }

    let roleType = await roleTypeService.get(roleTypeCode, context)

    employee.role = await roleService.getOrCreate({
        type: roleType,
        user: user,
        employee: employee,
        organization: context.organization,
        status: roleStatus
    }, context)

    context.processSync = true
    await offline.queue('employee', 'update', {
        id: employee.id
    }, context)

    return employee
}

const update = async (model, entity, context) => {
    let log = context.logger.start('services/employees:update')

    if (model.reason) {
        entity.reason = model.reason.toLowerCase()
    }
    if (model.reason && (model.dol && model.dol !== entity.dol)) {
        entity.dol = model.dol

        if (dates.date(entity.dol).isPast()) {
            model.status = 'inactive'
        }
    }
    if (model.doj) {
        entity.doj = model.doj
    }
    if (model.config && model.config.biometricId) {
        model.config.biometricCode = model.config.biometricId
    }

    if (model.status && model.status !== entity.status) {
        entity.status = model.status
        if (entity.status === 'inactive') {
            entity.dol = model.dol || new Date()
            entity.reason = model.reason
        }
        if (entity.status === 'active') {
            entity.dol = null
            entity.reason = null
            entity.doj = entity.doj || new Date()
        }
    }

    if (model.phone) {
        entity.phone = model.phone
    }

    if (model.email) {
        entity.email = model.email
    } else if (!model.email && !entity.email) {
        entity.email = `${model.code}@${context.organization.code}.com`
    }

    if (model.profile) {
        entity.profile = updateProfile(model.profile, entity.profile, context)
    }

    if (model.config) {
        entity.config = entity.config || {}
        Object.keys(model.config).forEach(key => {
            entity.config[key] = model.config[key]
        })
        entity.markModified('config')
    }

    if (model.address) {
        entity.address = model.address
    }

    if (model.status) {
        entity.status = model.status
    }

    if (model.type) {
        entity.type = model.type
    }

    if (model.supervisor) {
        entity.supervisor = await get(model.supervisor, context)
    }

    if (model.division) {
        entity.division = await divisionService.get(model.division, context)
    }

    if (model.designation) {
        entity.designation = await designationService.get(model.designation, context)
    }

    if (model.department) {
        entity.department = await departmentService.get(model.department, context)
    }

    if (context.tenant.code === 'aqua') {
        const userModel = {
            phone: entity.phone,
            email: entity.email,
            profile: entity.profile
        }

        if (model.password) {
            userModel.password = model.password
        }

        entity.user = await userService.update(userModel, entity.user, context)
    }

    await entity.save()

    context.processSync = true
    await offline.queue('employee', 'update', {
        id: entity.id
    }, context)

    log.end()
    return getById(entity.id, context)
}

const search = (query, context) => {
    context.logger.start('search')
    query = query || {}

    query.organization = context.organization.id

    return db.employee.find(query).sort({
        'profile.firstName': 1
    }).populate('user department division designation organization').populate({
        path: 'supervisor',
        populate: {
            path: 'user department division designation organization'
        }
    })
}

const getById = async (id, context) => {
    context.logger.debug('service/employees:getById')
    return db.employee.findById(id).populate('user department division designation').populate({
        path: 'supervisor',
        populate: {
            path: 'user department division designation organization'
        }
    }).populate({
        path: 'organization',
        populate: {
            path: 'owner'
        }
    })
}

const getByCode = async (data, context) => {
    context.logger.debug('service/employees:getByCode')
    return db.employee.findOne({
        code: data.code || data,
        organization: context.organization.id
    }).populate('user department division designation organization').populate({
        path: 'supervisor',
        populate: {
            path: 'user department division designation organization'
        }
    })
}

const setSupervisor = async (employee, supervisor, context) => {
    context.logger.start('service/employees:setSupervisor')
    if (!supervisor) {
        return null
    }

    employee.supervisor = supervisor

    return employee.save()
}

const get = async (query, context) => {
    context.logger.debug('service/employees:get')

    if (typeof query === 'string') {
        if (query.isObjectId()) {
            return getById(query, context)
        } else {
            return getByCode(query, context)
        }
    }
    if (query.id) {
        return getById(query.id, context)
    }

    if (query.code) {
        return getByCode(query.code, context)
    }
    return null
}

const getOrCreate = async (model, context) => {
    context.logger.start('services/employees: getOrCreate')

    let employee

    if (model.id) {
        employee = await db.employee.findById(model.id).populate('user department division designation organization').populate({
            path: 'supervisor',
            populate: {
                path: 'user department division designation organization'
            }
        })
    }

    if (employee) {
        return employee
    }

    if (model.code) {
        employee = await db.employee.findOne({
            code: model.code,
            organization: context.organization.id
        }).populate('user department division designation organization').populate({
            path: 'supervisor',
            populate: {
                path: 'user department division designation organization'
            }
        })
    }

    if (employee) {
        return employee
    }

    let user = await userService.getOrCreate({
        phone: model.phone,
        email: model.email
    }, context)

    employee = await db.employee.findOne({
        user: user,
        organization: context.organization.id
    }).populate('user department division designation organization').populate({
        path: 'supervisor',
        populate: {
            path: 'user department division designation organization'
        }
    })

    if (employee) {
        return employee
    }

    let profile = model.profile || user.profile || {}
    profile.pic = profile.pic || {}
    profile.pic.url = profile.pic.url || undefined
    profile.pic.thumbnail = profile.pic.thumbnail || undefined
    profile.gender = profile.gender || undefined

    let data = {
        code: model.code,
        phone: model.phone || user.phone,
        email: model.email || user.email,
        profile: profile,
        address: model.address || user.address,
        supervisor: model.supervisor,
        division: model.division || '',
        designation: model.designation || model.designation || '',
        department: model.department || '',
        user: user,
        type: model.type,
        status: model.status || 'active'
    }

    employee = await create(data, context)

    let roleTypeCode = `${context.organization.type || 'organization'}.${employee.type}`
    let roleStatus

    if (model.role) {
        if (model.role.type || model.role.code) {
            roleTypeCode = model.role.type || model.role.code
        }
        if (model.role.status) {
            roleStatus = model.role.status
        }
    }

    let roleType = await roleTypeService.get(roleTypeCode, context)

    await roleService.getOrCreate({
        type: roleType,
        user: user,
        employee: employee,
        status: roleStatus
    }, context)

    context.processSync = true
    await offline.queue('employee', 'update', {
        id: employee.id
    }, context)

    return employee
}

const remove = async (id, context) => {
    context.logger.start('services:employees:remove')

    let employee = await db.employee.findById(id)

    // find role and inactive role

    return update({
        status: 'inactive'
    }, employee, context)
}

exports.create = create
exports.update = update
exports.get = get
exports.getByCode = getByCode
exports.getById = getById
exports.search = search
exports.setSupervisor = setSupervisor
exports.getOrCreate = getOrCreate
exports.remove = remove
