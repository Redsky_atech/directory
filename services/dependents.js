'use strict'
const db = require('../models')
const userService = require('./users')
const roleService = require('./roles')

const create = async (model, roleId, context) => {
    let log = context.logger.start('services/dependents:create')

    let role = await db.role.findById(roleId)

    if (!role) {
        throw new Error('dependent head role not found')
    }

    let dependentUser = null
    let dependentRole = null

    if (model.role.id) {
        dependentRole = await db.role.findById(model.role.id)
    } else {
        let query = {
            $or: []
        }

        if (model.role.user.phone) {
            query.$or.push({ phone: model.role.user.phone })
        }

        if (model.role.user.email) {
            query.$or.push({ email: model.role.user.email })
        }

        if (query.$or.length) {
            dependentUser = await db.user.findOne(query)
        }

        if (!dependentUser) {
            dependentUser = await userService.create(model.role.user, context)
        } else {
            dependentUser = await userService.update(model.role.user, dependentUser, context)
        }

        dependentRole = await db.role.findOne({
            user: dependentUser.id,
            organization: { $exists: false },
            employee: { $exists: false }
        }) || await roleService.create({
            user: dependentUser,
            tenant: context.tenant.id
        }, context)
    }

    if (!dependentRole) {
        throw new Error('dependent role not found')
    }

    log.end()
    return roleService.update({
        dependents: [{
            role: dependentRole,
            relation: model.relation
        }]
    }, role, context)
}

exports.create = create
