'use strict'
const logger = require('@open-age/logger')('services/organizations')
let updationScheme = require('../helpers/updateEntities')
const offline = require('@open-age/offline-processor')

const employeeService = require('../services/employees')
const divisionService = require('../services/divisions')
const designationService = require('../services/designations')
const departmentService = require('../services/departments')

const db = require('../models')

const create = async (data, context) => {
    let log = context.logger.start('create')
    let model = {
        name: data.name,
        code: data.code,
        shortName: data.shortName,
        type: data.type,
        location: data.location,
        address: data.address,
        status: data.status || 'new',
        tenant: context.tenant
    }
    let organization = await new db.organization(model).save()

    await context.setOrganization(organization)

    let employeeModel = data.employee || {}

    let division = await divisionService.create(employeeModel.division || {
        code: 'default', name: 'Default'
    }, context)
    let designation = await designationService.create(employeeModel.designation || {
        code: 'default', name: 'Default'
    }, context)
    let department = await departmentService.create(employeeModel.department || {
        code: 'default', name: 'Default'
    }, context)

    let employee = await employeeService.create({
        type: employeeModel.type,
        code: employeeModel.code,
        status: employeeModel.status,
        role: employeeModel.role,
        user: context.user,
        profile: employeeModel.profile || context.user.profile.toObject(),
        email: context.user.email,
        phone: context.user.phone,
        division: division,
        department: department,
        designation: designation
    }, context)

    log.debug(`employee created ${employee.id}`)

    organization.owner = employee.role
    await organization.save()

    // TODO:
    // if (!newRole) {
    //     let newRoleType = await roleTypeService.get(roleModel.type, context)

    //     newRole = await roleService.getOrCreate({
    //         type: newRoleType,
    //         employee: employee,
    //         user: context.user,
    //         status: roleModel.status
    //     }, context)
    // }

    // log.debug(`new role created ${newRole.id}`)

    // organization.owner = newRole

    context.processSync = true
    await context.setRole(employee.role)

    await offline.queue('organization', 'create', {
        id: organization.id
    }, context)

    organization.role = employee.role

    log.end()
    return organization
}

const getByIdOrCode = async (identifier, context) => {
    context.logger.start('services/organization:getByIdOrCode')

    let query = identifier.isObjectId() ? { _id: identifier } : { code: identifier }

    return db.organization.findOne(query)
}

const update = async (model, organization, context) => {
    let log = context.logger.start('services/organizations:update')

    let notifyToAdmin = ((model.status === 'active') && (organization.status === 'new'))

    if (model.name) {
        organization.name = model.name
    }
    if (model.code && (model.code !== organization.code)) {
        organization.previousCode = organization.code
        organization.isCodeUpdated = true
        organization.code = model.code
    }
    if (model.shortName) {
        organization.shortName = model.shortName
    }
    if (model.type) {
        organization.type = model.type
    }
    if (model.location) {
        organization.location = model.location
    }
    if (model.address) {
        organization.address = updationScheme.update(model.address, organization.address)
    }
    if (model.status) {
        organization.status = model.status
    }

    if (model.owner) {
        organization.owner = model.owner
    }

    let updatedOrg = await organization.save()

    context.processSync = true

    await offline.queue('organization', 'update', {
        id: updatedOrg.id
    }, context)

    if (notifyToAdmin) {
        offline.queue('organization', 'status', {
            id: updatedOrg.id
        }, context)
    }

    log.end()

    return db.organization.findById(updatedOrg.id)
        .populate({
            path: 'owner',
            populate: {
                path: 'designation division department'
            }
        })
}

const getById = async (id) => {
    logger.start('getById')

    return db.organization.findById(id).populate('owner')
}

const getByCode = async (code) => {
    logger.start('getByCode')

    return db.organization.findOne({ code: code })
}

const availableCodeFinder = async (existCode) => {
    if (typeof availableCodeFinder.num === 'undefined') {
        availableCodeFinder.num = 0
    }
    availableCodeFinder.num++
    let code = existCode + availableCodeFinder.num
    let organization = await db.organization.findOne({ code: code })

    if (!organization) { return code }
    return availableCodeFinder(code)
}

exports.create = create
exports.getByIdOrCode = getByIdOrCode
exports.update = update
exports.getById = getById
exports.getByCode = getByCode
exports.availableCodeFinder = availableCodeFinder
